//Classe main para instanciar as classes

public class TesteContas{
	
	public static void main(String[] args){
		
		Conta umaConta = new Conta();
		ContaCorrente umaContaCorrente = new ContaCorrente();
		ContaPoupanca umaContaPoupanca = new ContaPoupanca();
		
		umaConta.deposita(1000);
		umaContaCorrente.deposita(1000);
		umaContaPoupanca.deposita(1000);
	
		umaConta.atualiza(0.01);
		umaContaCorrente.atualiza(0.01);
                umaContaPoupanca.atualiza(0.01);

		System.out.println(umaConta.getSaldo());
		System.out.println(umaContaCorrente.getSaldo());
		System.out.println(umaContaPoupanca.getSaldo());
	}
}

//4) Acontece que o valor que ContaCorrente retorna não será o desejado, pois no fim dos cálculos ele retornará uma diferença de 0.002 centavos do valor inserido devido a erros.
